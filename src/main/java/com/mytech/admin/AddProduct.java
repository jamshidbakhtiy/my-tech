package com.mytech.admin;

import com.mytech.bot.model.Product;
import com.mytech.bot.service.IOService;
import com.mytech.bot.service.ProductService;
import com.mytech.bot.util.ScannerUtil;

import java.util.InputMismatchException;
import java.util.List;

public class AddProduct {
    public static IOService ioService = new IOService();

    public static void addProduct() {
        List<String> categories = ioService.getCategoryList();
        while (true) {
            System.out.println("""
                    1. Add product
                    2. Add category""");
            String operation = ScannerUtil.getString("?: ");
            if (operation.equals("1")) {
                for (String category : categories) {
                    System.out.println(category);
                }
                try {
                    String category = ScannerUtil.getString("Enter category: ");
                    boolean check = categories.stream().anyMatch(s -> s.equalsIgnoreCase(category));

                    if (!check) {
                        System.out.println("\033[0;91mWrong category\033[0m");
                        continue;
                    }
                    String name = ScannerUtil.getString("Enter name: ");
                    String imageURL = ScannerUtil.getString("Enter imageURL: ");
                    String description = ScannerUtil.getString("Enter description: ");

                    Double price = ScannerUtil.getDouble("Enter price");

                    Product product = new Product(name, imageURL, description, price, category);
                    ioService.addProduct(product);
                } catch (InputMismatchException e) {
                    System.out.println("\033[0;91mWrong price\033[0m");
                }
            }
            if (operation.equals("2")){
                String category = ScannerUtil.getString("Enter category name: ");
                ioService.addCategory(category);
            }
        }
    }
}
