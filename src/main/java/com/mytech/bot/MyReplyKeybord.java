package com.mytech.bot;

import com.mytech.bot.model.Product;
import com.mytech.bot.service.IOService;
import com.mytech.bot.util.BotConstants;
import lombok.Builder;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.mytech.bot.MyBot.BACKS;
import static com.mytech.bot.MyBot.SAVATCHA;
import static com.mytech.bot.util.BotConstants.*;

public class MyReplyKeybord {
    public static int number = 0;
    public static IOService ioService = new IOService();

    public static ReplyKeyboard createReplyKeyboardMarkup(List<String> keyboard) {
        if (keyboard.equals(ioService.getCategoryList())) {
            keyboard.add(BACK);
        }
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();

        KeyboardRow row = new KeyboardRow();
        List<KeyboardRow> rows = new ArrayList<>();
        for (int i = 1; i <= keyboard.size(); i++) {
            row.add(new KeyboardButton(keyboard.get(i - 1)));
            if (i % 2 == 0) {
                rows.add(row);
                row = new KeyboardRow();
            }
        }
        if (keyboard.size() % 2 != 0) {
            rows.add(row);
        }

        replyKeyboardMarkup.setKeyboard(rows);
        replyKeyboardMarkup.setResizeKeyboard(true);
        return replyKeyboardMarkup;
    }

    public static InlineKeyboardMarkup createInlineKeyboardMarkup(List<Product> product) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        String parentId = null;
        for (int i = 1; i <= product.size(); i++) {
            Product product1 = product.get(i - 1);
            InlineKeyboardButton button = new InlineKeyboardButton(product1.getName());
            button.setCallbackData(String.valueOf(product1.getId()));
            row.add(button);
            parentId = String.valueOf(product1.getId());
            if (i % 2 == 0) {
                rows.add(row);
                row = new ArrayList<>();
            }
        }
        if (product.size() % 2 != 0) {
            rows.add(row);
        }
        if (!product.stream().allMatch(products -> products.getId() == null)) {
            InlineKeyboardButton button = new InlineKeyboardButton("Back");

            button.setCallbackData("back" + parentId);
            row = new ArrayList<>();
            row.add(button);
            rows.add(row);
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        return inlineKeyboardMarkup;
    }


    public static ReplyKeyboard createInlineKeyboardMarkup(String text) {
        List<Product> products = ioService.getProduct();
        List<Product> list = products.stream().filter(product -> Objects.equals(product.getCategory(), text)).toList();
        return createInlineKeyboardMarkup(list);
    }


    public static ReplyKeyboard createInlineKeyboardMarkupEn(String productId) {

        if (productId.startsWith(BACK)) {
            return createInlineKeyboardMarkup(ioService.getProduct());
        }

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        InlineKeyboardButton button = new InlineKeyboardButton();


        if (productId.startsWith("-") && number != 0) {
            number--;
        } else if (productId.startsWith("+")) {
            number++;
        }

        productId = productId.substring(productId.length() - 36);
        button.setText("+");
        button.setCallbackData("+" + productId);

        InlineKeyboardButton button1 = new InlineKeyboardButton();
        button1.setText(BACKS);
        button1.setCallbackData(BACK + productId);

        InlineKeyboardButton button2 = new InlineKeyboardButton();
        button2.setText(SAVATCHA);
        button2.setCallbackData(BotConstants.SAVAT + number + productId);

        InlineKeyboardButton button3 = new InlineKeyboardButton();
        button3.setText("-");
        button3.setCallbackData("-" + productId);

        InlineKeyboardButton button4 = new InlineKeyboardButton();
        button4.setText("" + (number));
        button4.setCallbackData(productId);

        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();

        Collections.addAll(row1, button3, button4, button);
        Collections.addAll(row2, button1, button2);
        Collections.addAll(rows, row1, row2);

        inlineKeyboardMarkup.setKeyboard(rows);
        return inlineKeyboardMarkup;

    }

}
