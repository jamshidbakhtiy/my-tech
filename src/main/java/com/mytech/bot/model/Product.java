package com.mytech.bot.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private UUID id;
    private String name;
    private String imageUrl;
    private String description;
    private double price;
    private String category;

    public Product(String name, String imageUrl, String description, double price, String category) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.category = category;
    }
}
