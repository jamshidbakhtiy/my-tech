package com.mytech.bot.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private UUID id;
    private Product product;
    private Long chatId;
    private double totalPrice;
    private String date;
}
