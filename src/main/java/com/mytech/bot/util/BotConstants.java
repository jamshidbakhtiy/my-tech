package com.mytech.bot.util;

import java.util.List;

public interface BotConstants {
    String TOKEN = "6358879600:AAH1FoXgbm7QNVitCSkXzwZS1Fo8RnHbzt4";
    String USERNAME = "http://t.me/mytechteam_bot";
    String BACK = "BACK";
    String SAVAT = "SAVAT";
    String BASKET ="BASKET";

    List<String>language = List.of("Uzbek","Russian","English");
    List<String> mainCategoryEng= List.of("\uD83C\uDF7D Menu", "\uD83D\uDCD6 Order history",
            "✍\uFE0F Comment", "ℹ\uFE0F Information", "☎\uFE0F Contact us","Setting");
    List<String> COMMENTEng = List.of(
            "Everything is great 🤩🤗😊",
            "Good 😉😊",
            "Did not like 😏😮",
            "bad 🤬");
    String ORQAGA = "ORQAGA";
    List<String> mainCategoryUz = List.of("\uD83C\uDF7D Menu", "\uD83D\uDCD6 Buyurtma tarixi",
            "✍\uFE0F Fikr bildirish", "ℹ\uFE0F Ma'lumot", "☎\uFE0F Biz bilan Aloqa"," Sozlash");

    List<String> COMMENTUz = List.of(
            "Juda Ajoyib 🤗🤩",
            "Yaxshi 🙃🙂",
            "Yoqmadi 😮😏",
            "yomon 🤬");
    String НАЗАД = "назад";
    String КОРЗИНА = "Корзина";
    List<String> mainCategoryRu = List.of("\uD83C\uDF7D Меню", "\uD83D\uDCD6 История заказов",
            "✍\uFE0F Комментарий", "ℹ\uFE0F Информация", "☎\uFE0F Связаться с нами","корректирование");
    List<String> COMMENTRu = List.of(
            "Все отлично ☺️🤗🤩",
            "Хороший 🙂",
            "Не нравится 😔🙃",
            "плохой 🤬");


}
