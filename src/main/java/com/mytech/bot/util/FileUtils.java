package com.mytech.bot.util;

import lombok.SneakyThrows;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class FileUtils {
    @SneakyThrows
    public static void write(String path, String data){
        Files.writeString(Path.of(path),data, StandardOpenOption.WRITE);
    }
}
