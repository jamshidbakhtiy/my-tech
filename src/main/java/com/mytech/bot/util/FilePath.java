package com.mytech.bot.util;

public class FilePath {
    public static String PATH_CATEGORY_LIST = "src/main/resources/categoryList.json";
    public static String PATH_CATEGORY_LIST_Ru = "src/main/resources/categoryListRu.json";
    public static String PATH_CATEGORY_LIST_Uz = "src/main/resources/categoryListUz.json";
    public static String PATH_PRODUCTS = "src/main/resources/products.json";
    public static String PATH_USERS = "src/main/resources/users.json";
    public static String PATH_ORDERS = "src/main/resources/orders.json";
}
