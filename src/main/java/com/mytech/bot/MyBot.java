package com.mytech.bot;

import com.mytech.bot.convert.UserConvert;
import com.mytech.bot.model.Order;
import com.mytech.bot.model.Product;
import com.mytech.bot.model.User;
import com.mytech.bot.service.IOService;
import com.mytech.bot.service.OrderSevice;
import com.mytech.bot.service.ProductService;
import com.mytech.bot.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static com.mytech.bot.MyReplyKeybord.*;
import static com.mytech.bot.util.BotConstants.*;

public class MyBot extends TelegramLongPollingBot {
    private static final UserService userService = new UserService();
    private static final ProductService productService = new ProductService();
    private static final UserConvert userConverter = new UserConvert();
    private static final OrderSevice orderSevice = new OrderSevice();
    private static final IOService ioService = new IOService();
    public static List<String> productList = ioService.getCategoryList();
    public static List<String> productListUz = ioService.getCategoryListUz();
    public static List<String> productListRu = ioService.getCategoryListRu();
    public static List<String> mainCategory = new ArrayList<>();
    public static List<String> COMMENT = new ArrayList<>();
    public static String BACKS;
    public static String SAVATCHA;
    public static List<String> PRODUCTLIST = new ArrayList<>();

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Long chatId = update.getMessage().getChatId();
            Contact contact = update.getMessage().getContact();
            boolean exist = userService.existByChatId(chatId);
            if (Objects.nonNull(contact)) {
                User user = userConverter.convertToEntity(contact);
                ioService.addUser(user);
                myExecuteCategory(chatId, "Successfully registered", createReplyKeyboardMarkup(mainCategoryEng));
                return;
            }
            if (!exist) {
                myExecuteCategory(chatId, "Welcome to our bot !", shareContact());
                return;
            }
            if (StringUtils.equals(update.getMessage().getText(), "/start")) {
                myExecuteCategory(chatId, "Welcome to the bot please select a language",
                        createReplyKeyboardMarkup(language));
            }
            if (update.getMessage().getText().equals("Uzbek")) {
                mainCategory = mainCategoryUz;
                COMMENT = COMMENTUz;
                BACKS = ORQAGA;
                SAVATCHA = SAVAT;
                PRODUCTLIST = productListUz;
                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(mainCategory));
            }
            if (update.getMessage().getText().equals("Russian")) {
                mainCategory = mainCategoryRu;
                COMMENT = COMMENTRu;
                BACKS = НАЗАД;
                SAVATCHA = КОРЗИНА;
                PRODUCTLIST = productListRu;
                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(mainCategory));
            }
            if (update.getMessage().getText().equals("English")) {
                mainCategory = mainCategoryEng;
                COMMENT = COMMENTEng;
                BACKS = BACK;
                SAVATCHA = BACK;
                PRODUCTLIST = productList;
                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(mainCategory));
            }

            if (StringUtils.equalsAnyIgnoreCase((update.getMessage().getText()), BACK)) {
                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(mainCategory));
            }
            String text = update.getMessage().getText();
            if (StringUtils.equalsAnyIgnoreCase(text, mainCategory.get(0))) {
                myExecuteCategory(chatId, "Share location !", shareLocation());

                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(PRODUCTLIST));
            }
            if (StringUtils.equalsAnyIgnoreCase(text, mainCategory.get(1))) {
                String orderByUserId = orderSevice.getOrderByUserId(chatId);
                myExecuteCategory(chatId, orderByUserId, createReplyKeyboardMarkup(mainCategory));
            }
            if (StringUtils.equalsAnyIgnoreCase(text, mainCategory.get(2))) {
                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(COMMENT));
            }
            if (StringUtils.equalsAnyIgnoreCase(text, mainCategory.get(3))) {
                myExecuteCategory(chatId, "choose category", createReplyKeyboardMarkup(PRODUCTLIST));
            }
            if (StringUtils.equalsAnyIgnoreCase(text, mainCategory.get(4))) {
                myExecuteCategory(chatId, "If you have any Questions/Complaints/Suggestions you can write to us\n" +
                        "☎\uFE0F Telefon raqam: 71-200-73-73 / 71 200-86-86", createReplyKeyboardMarkup(mainCategory));
            }
            if (productList.contains(text)) {
                myExecuteCategory(chatId, "Choose one...", createInlineKeyboardMarkup(text));
            }


            if (StringUtils.equalsAnyIgnoreCase((update.getMessage().getText()), BACKS)) {
                myExecuteCategory(chatId, "category tanlang ", createReplyKeyboardMarkup(mainCategory));

            }
        } else if (update.hasCallbackQuery()) {
            String data = update.getCallbackQuery().getData();
            Long chatId = update.getCallbackQuery().getMessage().getChatId();
            DeleteMessage deleteMessage = new DeleteMessage();
            deleteMessage.setChatId(chatId.toString());
            deleteMessage.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            try {
                execute(deleteMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            if (data.startsWith(BACK)) {
                Product product = productService.getProductById(data.substring(4));
                myExecuteCategory(chatId, "Choose one...", createInlineKeyboardMarkupEn(product.getCategory()));
            }
            if (data.startsWith(BASKET)) {
                String substring = data.substring(6);
                Product product = productService.getProductById(substring);
                Order order = new Order(UUID.randomUUID(), product, chatId, product.getPrice(), LocalDate.now().toString());
                ioService.addOrder(order);
            } else {
                String substring = data.substring(data.length() - 36);
                myExecute(chatId, UUID.fromString(substring), createInlineKeyboardMarkupEn((data)));
            }

        }

    }

    private ReplyKeyboard shareLocation() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardButton phoneButton = new KeyboardButton();
        phoneButton.setText("share location !!!");
        phoneButton.setRequestLocation(true);
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(phoneButton);
        keyboardMarkup.setResizeKeyboard(true);
        List<KeyboardRow> keyboardRows = List.of(keyboardRow);
        keyboardMarkup.setKeyboard(keyboardRows);
        return keyboardMarkup;
    }

    private ReplyKeyboard shareContact() {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        KeyboardButton phoneButton = new KeyboardButton();
        phoneButton.setText("share Contact !!!");
        phoneButton.setRequestContact(true);
        KeyboardRow keyboardRow = new KeyboardRow();
        keyboardRow.add(phoneButton);
        keyboardMarkup.setResizeKeyboard(true);
        List<KeyboardRow> keyboardRows = List.of(keyboardRow);
        keyboardMarkup.setKeyboard(keyboardRows);
        return keyboardMarkup;
    }

    private void myExecuteCategory(Long chatId, String message, ReplyKeyboard r) {
        SendMessage s = new SendMessage();
        s.setChatId(chatId);
        s.setText(message);
        s.setReplyMarkup(r);
        try {
            execute(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void myExecute(Long chatId, UUID productId, ReplyKeyboard r) {
        Product products = ioService.getProduct().stream()
                .filter(product -> Objects.equals(product.getId(), productId))
                .findFirst().orElseThrow();
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChatId(chatId);
        sendPhoto.setPhoto(new InputFile(new File("D:/Sevinch PDP/For Me Java/Modul 5/My_tech/src/main/resources/d4dc3d8e.jpg")));
        sendPhoto.setCaption(products.getDescription());
        sendPhoto.setReplyMarkup(r);
        try {
            execute(sendPhoto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public String getBotUsername() {
        return USERNAME;
    }

    @Override
    public String getBotToken() {
        return TOKEN;
    }
}
