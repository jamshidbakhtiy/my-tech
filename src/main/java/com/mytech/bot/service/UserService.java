package com.mytech.bot.service;

import com.mytech.bot.model.User;

import java.util.List;
import java.util.Objects;

public class UserService {
    IOService ioService = new IOService();

    public boolean existByChatId(Long chatId) {
        List<User> users = ioService.getUser();
        return users.stream().anyMatch(user -> Objects.equals(user.getChatId(),chatId));
    }
}
