package com.mytech.bot.service;

import com.mytech.bot.model.Order;

import java.util.List;
import java.util.stream.Collectors;

public class OrderSevice {
    private static IOService ioService = new IOService();
    public String getOrderByUserId(Long chatId){
        List<Order> orders = IOService.getOrder();
        List<Order> collect = orders.stream().filter(order -> order.getChatId().equals(chatId)).toList();
        StringBuilder sb = new StringBuilder();
        for (Order order : orders) {
            sb.append("Date : "+order.getDate()+" Name : "+order.getProduct().getName()+" Cost : "+order.getProduct().getPrice()+
            " description"+order.getProduct().getDescription());
        }
        return String.valueOf(sb);
    }
}
