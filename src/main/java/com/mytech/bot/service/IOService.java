package com.mytech.bot.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytech.bot.model.Order;
import com.mytech.bot.model.Product;
import com.mytech.bot.model.User;
import com.mytech.bot.util.FilePath;
import com.mytech.bot.util.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.codec.binary.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class IOService {
    public static ObjectMapper objectMapper = new ObjectMapper();

    @SneakyThrows
    public List<String> getCategoryList() {
        return objectMapper.readValue(new File(FilePath.PATH_CATEGORY_LIST), new TypeReference<List<String>>() {
        });
    }

    @SneakyThrows
    public List<String> getCategoryListRu() {
        return objectMapper.readValue(new File(FilePath.PATH_CATEGORY_LIST_Ru), new TypeReference<List<String>>() {
        });
    }

    @SneakyThrows
    public List<String> getCategoryListUz() {
        return objectMapper.readValue(new File(FilePath.PATH_CATEGORY_LIST_Uz), new TypeReference<List<String>>() {
        });
    }

    @SneakyThrows
    public List<Product> getProduct() {
        return objectMapper.readValue(new File(FilePath.PATH_PRODUCTS), new TypeReference<List<Product>>() {
        });
    }

    @SneakyThrows
    public List<User> getUser() {
        return objectMapper.readValue(new File(FilePath.PATH_USERS), new TypeReference<>() {
        });
    }


    @SneakyThrows
    public static List<Order> getOrder() {
        return objectMapper.readValue(new File(FilePath.PATH_ORDERS), new TypeReference<>() {
        });
    }

    @SneakyThrows
    public void addUser(User user) {
        List<User> users = getUser();

        validateUser(user, users);

        user.setId(UUID.randomUUID());
        users.add(user);
        String usersStringJson = objectMapper.writeValueAsString(users);
        FileUtils.write(FilePath.PATH_USERS, usersStringJson);
    }

    @SneakyThrows
    public void addCategory(String category) {
        List<String> categories = getCategoryList();
        categories.add(category);

        String productsStringJson = objectMapper.writeValueAsString(categories);
        FileUtils.write(FilePath.PATH_CATEGORY_LIST, productsStringJson);
    }

    private void validateUser(User user, List<User> users) {
        users.stream().filter(u -> StringUtils.equals(u.getPhoneNumber(), user.getPhoneNumber()))
                .findFirst().ifPresent(u -> {
                    throw new IllegalArgumentException(String.format("%s user exists", u.getPhoneNumber()));
                });
    }

    @SneakyThrows
    public void addProduct(Product product) {
        List<Product> products = getProduct();
        product.setId(UUID.randomUUID());
        products.add(product);
        String productsStringJson = objectMapper.writeValueAsString(products);
        FileUtils.write(FilePath.PATH_PRODUCTS, productsStringJson);
    }


    @SneakyThrows
    public void addOrder(Order order) {
        List<Order> orders = getOrder();
        order.setId(UUID.randomUUID());
        orders.add(order);
        String ordersStringJson = objectMapper.writeValueAsString(orders);

        FileUtils.write(FilePath.PATH_ORDERS, ordersStringJson);
    }

}
