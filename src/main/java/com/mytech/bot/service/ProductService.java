package com.mytech.bot.service;

import com.mytech.bot.model.Product;

import java.util.Objects;
import java.util.UUID;

public class ProductService {
    IOService ioService = new IOService();
    public Product getProductById(String productId) {
        System.out.println(productId.length());
        UUID id = UUID.fromString(productId);
        Product product1 = ioService.getProduct().stream().filter(product -> Objects.equals(product.getId(), id)).findFirst().get();
        return product1;
    }

}
