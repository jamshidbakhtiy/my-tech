package com.mytech.bot.convert;

import com.mytech.bot.model.User;
import org.telegram.telegrambots.meta.api.objects.Contact;

public class UserConvert {
    public User convertToEntity(Contact contact){
        return User.builder()
                .phoneNumber(contact.getPhoneNumber())
                .firstName(contact.getFirstName())
                .lastName(contact.getLastName())
                .chatId(contact.getUserId())
                .build();
    }
}
